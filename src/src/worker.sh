#!/bin/bash

export LD_LIBRARY_PATH=/home/mtasr/Desktop/mcloud_wrapper/src/linux_lib64
export PYTHONPATH=/home/mtasr/LT2020/python-asr/pynn/src

pythonCMD="python -u -W ignore"

$pythonCMD python_worker_2.py --dict "model/bpe4k.dic" --model "model/epoch-avg.pt" --beam-size 4 --downsample 1 --use-cnn --freq-kn 3 --freq-std 2 --n-classes 4003 --d-input 40 --d-model 1024 -fi di -fo di --n-enc 6 --space  ▁  --n-dec 2 --shared-emb
