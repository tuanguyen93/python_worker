# cython: language_level=3
from MCloud import *
from S2STime import *
from WorkerUserData import *
import subprocess
from MCloudPacketRCV import MCloudPacketRCV
import os
import torch
import numpy as np
import pickle
import time
import threading
from  decode_asr import *
from pynn.util.decoder import Decoder
from pynn.net.tf import Transformer
from pynn.net.seq2seq import Seq2Seq
import argparse
import re
import sys
TEMP_DIR = "/tmp/recognizer"
cache_text = ""

def segment(mcloud):


    err = 0
    # connect to mediator
    res_connect = mcloud.connect(serverHost.encode("utf-8"), serverPort)
    i = 0
    if res_connect == 1:
        print("ERROR Connection established")
    else:
        print("WORKER INFO Connection established ==> waiting for clients.")

    while res_connect == 0:
        # wait for client
        res = mcloud.wait_for_client(stream_id.encode("utf-8"))

        proceed = False
        if res == 1:
            print("WORKER ERROR while waiting for client")
            break
        elif res == 0:

            proceed = True
            print("WORKER INFO received client request ==> waiting for packages")
        while (proceed):

            packet = MCloudPacketRCV(mcloud)

            type = packet.packet_type()
            if packet.packet_type() == 3:

                mcloud.process_data_async(packet, data_callback)

            elif packet.packet_type() == 7:  # MCloudFlush
                """
                a flush message has been received -> wait (block) until all pending packages
                from the processing queue has been processed -> finalizeCallback will
                be called-> flush message will be passed to subsequent components
                """
                mcloud.wait_for_finish(0, "processing")
                mcloud.send_flush()
                print("WORKER INFO received flush message ==> waiting for packages.")
                mcloudpacketdenit(packet)
                break
            elif packet.packet_type() == 4:  # MCloudDone

                print("WOKRER INFO received DONE message ==> waiting for clients.")
                mcloud.wait_for_finish(1, "processing")
                mcloudpacketdenit(packet)
                clean(tempDir)
                proceed = False
            elif packet.packet_type() == 5:  # MCloudError
                # In case of a error or reset message, the processing is stopped immediately by
                # calling mcloudBreak followed by exiting the thread.
                mcloud.wait_for_finish(1, "processing")
                mcloud.stop_processing("processing")
                mcloudpacketdenit(packet)
                clean(tempDir)
                print("WORKER INFO received ERROR message >>> waiting for clients.")
                proceed = False
            elif packet.packet_type() == 6:  # MCloudReset
                mcloud.stop_processing("processing")

                print("CLIENT INFO received RESET message >>> waiting for clients.")
                mcloudpacketdenit(packet)
                clean(tempDir)
                proceed = False
            else:
                print("CLIENT ERROR unknown packet type {!s}".format(packet.packet_type()))
                proceed = False
                err = 1
            if err == 1:
                break
        print("WORKER WARN connection terminated ==> trying to reconnect.")

def printusage():
    print("\n")
    print("\nNAME\n\t%s - Speech Recognition Backend.\n");
    print("\nSYNOPSIS\n\t%s [OPTION]... ASRCONFIGFILE\n");
    print("\nDESCRIPTION\n""\tThis is a example implementation of an ASR backend which connects to""\tMTEC's Mediator in the cloud.\n");
    print("\nOPTION\n""\t-s, --server=HOSTNAME\n\t\tHost name of the server where the Mediator is running.\n\n""\t-p, --serverPort=PORT\n\t\tPort address at which the Mediator accepts worker.\n\n""\t-h, --help\n\t\tShows this help.\n\n")
    return 0

def processing_finalize_callback():
    print("INFO in processing finalize callback")
    global proc
    proc.stdin.close()
    proc.terminate()

def processing_error_callback():
    print("INFO In processing error callback")
    global proc
    proc.stdin.close()
    proc.terminate()


def processing_break_callback():
    print("INFO in processing break callback")
    global proc
    proc.stdin.close()
    proc.terminate()


def init_callback():

    clean(tempDir)
    global proc
    global cache_text
    args = "-audio %s/recording.adc -tmpDir %s -file -"
    path = os.path.dirname(os.path.realpath('__file__'))
    segmenter = path + "/segmenter"
    params = (args % (TEMP_DIR, TEMP_DIR)).split()
    params.insert(0, segmenter)
    cache_text = ""
    proc = subprocess.Popen(params,
                            universal_newlines=False,
                              stdin=subprocess.PIPE)
    print("INFO in processing init callback ")

def data_callback(i,sampleA):
    sample = np.asarray(sampleA,dtype=np.int16)
    global cache_text
    global proc
    proc.stdin.write(sample.tobytes())
    return  0

def clean(tempDir):

    if os.path.exists(tempDir):
        subprocess.call(["rm -rf %s/*.fgets" % tempDir], shell=True)
        subprocess.call(["rm -rf %s/*.idx" % tempDir], shell=True)
        subprocess.call(["rm -rf %s/*ready" % tempDir], shell=True)
        subprocess.call(["rm -rf %s/*start" % tempDir], shell=True)
        subprocess.call(["rm -rf %s/*info" % tempDir], shell=True)


parser = argparse.ArgumentParser(description='pynn')
#model argument
parser.add_argument('--n-classes', type=int, required=True)
parser.add_argument('--n-head', type=int, default=8)
parser.add_argument('--n-enc', type=int, default=4)
parser.add_argument('--n-dec', type=int, default=2)
parser.add_argument('--d-input', type=int, default=40)
parser.add_argument('--d-model', type=int, default=320)
parser.add_argument('--time-ds', help='downsample in time axis', type=int, default=1)
parser.add_argument('--use-cnn', help='use CNN filters', action='store_true')
parser.add_argument('--freq-kn', help='frequency kernel', type=int, default=3)
parser.add_argument('--freq-std', help='frequency stride', type=int, default=2)
parser.add_argument('--shared-emb', help='sharing decoder embedding', action='store_true')
parser.add_argument('--model', help='model file', required=True)
parser.add_argument('--dict', help='dictionary file', required=True)
parser.add_argument('--word-dict', help='word dictionary file', default=None)
parser.add_argument('--downsample', help='concated frames', type=int, default=1)
parser.add_argument('--mean-sub', help='mean subtraction', action='store_true')
parser.add_argument('--space', help='space token', type=str, default='?')
parser.add_argument('--beam-size', help='beam size', type=int, default=4)
parser.add_argument('--max-len', help='max len', type=int, default=200)
parser.add_argument('--device', help='device', type=str, default='cuda')
#worker argument
parser.add_argument('-s','--server', type=str, default="i13srv53.ira.uka.de")
parser.add_argument('-p','--port' ,type=int, default=60019)
parser.add_argument('-fi','--fingerprint', type=str, default="en-EU")
parser.add_argument('-fo','--outfingerprint',type=str, default="en-EU")
parser.add_argument('-i','--inputType' ,type=str, default="audio")
parser.add_argument('-o','--outputType', type=str, default="unseg-text")
args = parser.parse_args()


serverHost = args.server
serverPort = args.port
inputFingerPrint  = args.fingerprint
inputType         = args.inputType
outputFingerPrint = args.outfingerprint
outputType        = args.outputType
specifier           = ""
stream_id = ""
tempDir = TEMP_DIR

print("Initialize the Transformer model...")
model, device, dic, word_dic, args = init_asr_model(args)
print("Done.")
print("Waiting for audio to decode...")


print("#" * 40 + " >> TESTING MCLOUD WRAPPER API << " + "#" * 40)
mcloud_w = MCloudWrap("asr".encode("utf-8"), 1)
mcloud_w.add_service("MTEC asr".encode("utf-8"), "asr".encode("utf-8"), inputFingerPrint.encode("utf-8"), inputType.encode("utf-8"),outputFingerPrint.encode("utf-8"), outputType.encode("utf-8"), specifier.encode("utf-8"))
#set callback
mcloud_w.set_callback("init", init_callback)
mcloud_w.set_data_callback("worker")
mcloud_w.set_callback("finalize", processing_finalize_callback)
mcloud_w.set_callback("error", processing_error_callback)
mcloud_w.set_callback("break", processing_break_callback)
#clean tempfile
if os.path.exists(tempDir):
    subprocess.call(["rm -rf %s/*" % tempDir], shell=True)
else:
    os.mkdir(tempDir)


global proc

#segmentor thread
record = threading.Thread(target=segment, args=(mcloud_w,))
record.start()

cache_text = ""
tempDir = TEMP_DIR
recList = "%s/recording.adc.en-EN.fgets" % tempDir
#resending = True
seg_id_finish = -1

while True:
    if not os.path.exists(recList):
        continue

    f = open(tempDir + "/recording.adc.en-EN.idx", "r")
    number = f.read().strip()
    f.close()

    if number == "":
        continue

    if seg_id_finish + 1 < int(number):
        seg_id_finish = seg_id_finish + 1
        adc = tempDir + "/recording.adc.en-EN." + str(seg_id_finish)

    elif seg_id_finish == int(number):
        continue

    else:
        adc = tempDir + "/recording.adc.en-EN." + number
        seg_id_finish = int(number)


    previous_frame = 0

    adc_ready = '%s-ready' % adc
    adc_start = '%s-start' % adc
    adc_info = '%s-info' % adc

    if os.path.exists(adc_info):
        segment_file_info = open(adc_info, "r")
        seg_info = segment_file_info.read()
        segment_file_info.close()
    else:
        continue

    if seg_info == "":
        continue
    start_time = float(re.findall("startS \d+", seg_info)[0][7::])

    while not os.path.exists(adc_ready):
        if os.path.exists(adc_start):
            # decoding the audio segment
            hypo, previous_frame, start, end = decode(adc, model, device, dic, word_dic, args, start_time,previous_frame)
            list_words = hypo.strip().split()
            length = len(list_words)
            if hypo != cache_text:
                cache_text = hypo
            else:
                length = 0
            if os.path.exists(adc_info):
                mcloud_w.send_packet_result_async(start, end, list_words , length)

        else:
            if os.path.exists(adc_info):
                continue
            else:
                break
    if  os.path.exists(adc_ready):
        hypo, previous_frame, start, end =  decode(adc, model, device, dic, word_dic, args, start_time, previous_frame)
        list_words = hypo.strip().split()
        length = len(list_words)
        if hypo != cache_text:
            cache_text = hypo
        else:
            length = 0
        if os.path.exists(adc_info):
            mcloud_w.send_packet_result_async(start, end, list_words, length)

