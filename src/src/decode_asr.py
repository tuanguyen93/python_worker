#!/usr/bin/python

import time
import sys
import subprocess
import os
import io
import argparse
import math
import struct
import numpy
import torch
import re
from pynn.util.decoder import Decoder
from pynn.net.tf import Transformer
from pynn.net.seq2seq import Seq2Seq
import pickle

SAMPLE_RATE = 16000


def init_asr_model(args):

    device = torch.device(args.device)

    m_params = {'input_size': args.d_input,
                'hidden_size': args.d_model,
                'output_size': args.n_classes,
                'n_enc': args.n_enc,
                'n_dec': args.n_dec,
                'n_head': args.n_head,
                'time_ds': args.time_ds,
                'use_cnn': args.use_cnn,
                'freq_kn': args.freq_kn,
                'freq_std': args.freq_std,
                'shared_emb': args.shared_emb}

    model = Seq2Seq(**m_params).to(device)

    model.load_state_dict(torch.load(args.model))
    model.eval()
    fin = open(args.dict, 'r')
    dic = {}
    for line in fin:
        tokens = line.split()
        dic[int(tokens[1])] = tokens[0]
    word_dic = None
    if args.word_dict is not None:
        fin = open(args.word_dict, 'r')
        word_dic = {}
        for line in fin:
            tokens = line.split()
            word_dic[''.join(tokens[1:])] = tokens[0]
    return model, device, dic, word_dic, args


def _read_string(ark_file):
    s = ''
    while True:
        c = ark_file.read(1).decode('utf-8')
        if c == ' ' or c == '': return s
        s += c


def _read_integer(ark_file):
    n = ord(ark_file.read(1))
    return struct.unpack('>i', ark_file.read(n)[::-1])[0]


def down_sample(feature, ds=1):
    feature = feature[:(feature.shape[0] // ds) * ds, :]
    return feature.reshape(feature.shape[0] // ds, feature.shape[1] * ds)


def read_ark(ark_file, ds=1):
    ark_file = open(ark_file, 'rb')
    ark_file.seek(5)

    header = ark_file.read(2).decode('utf-8')
    if header != "\0B":
        print("Input .ark file is not binary");
        exit(1)
    format = _read_string(ark_file)

    if format == "FM":
        rows = _read_integer(ark_file)
        cols = _read_integer(ark_file)
        utt_mat = struct.unpack("<%df" % (rows * cols), ark_file.read(rows * cols * 4))
        utt_mat = numpy.array(utt_mat, dtype=numpy.float32)
        utt_mat = numpy.reshape(utt_mat, (rows, cols))
        utt_mat = down_sample(utt_mat, ds)
    else:
        print("Unsupported .ark file with %s format" % format);
        exit(1)
    ark_file.close()

    return utt_mat


def decode(adc, model, device, dic, word_dic, args, start_time, previous_frame):
    path = os.path.dirname(os.path.realpath(__file__))
    janus = "%s/janus.sh %s/feat-extract.tcl" % (path, path)
    feat = adc + ".ark"
    cmd = janus + " -adc %s -out %s" % (adc, feat)
    if not os.path.exists(adc):
        return

    start_time = start_time / SAMPLE_RATE * 1000
    subprocess.call([cmd], shell=True)
    while not os.path.exists(feat):
        time.sleep(0.1)
    if not os.path.exists(feat):
        return
    utt = read_ark(feat)
    seq = torch.FloatTensor(utt).unsqueeze(0)
    mask = torch.ones((1, seq.size(1)), dtype=torch.uint8)
    # size_seq =  (seq.size(1)*4)
    # if size_seq < 30 :
    #    return 0
    number_of_frame = (seq.size(1))
    print("Decoding for audio segment of %d frames" % number_of_frame)
    end_time = start_time + number_of_frame * 10
    start_time = start_time + previous_frame * 10

    with torch.no_grad():
        seq, mask = seq[:, previous_frame:, :].to(device), mask[:, previous_frame:].to(device)
        if seq.size(1) < 20:
            return "", previous_frame, start_time, end_time
        hypos, _ = Decoder.beam_search(model, seq, mask, device, args.beam_size, args.max_len)
        hypos = hypos.tolist()
        hypo = Decoder.hypo2text(hypos, dic, word_dic, args.space)[0]
        print("Recognized: " + hypo)
        hypo = hypo.replace("<unk>", "").replace("unk","").strip()



        if number_of_frame > previous_frame + 1000:
            previous_frame = number_of_frame

    return hypo, previous_frame,  start_time, end_time